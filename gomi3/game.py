'''The game module.

This module is in charge of handling a go game,
making sure that the board state is consistent and that player actions are valid.
'''


class InvalidMoveError(Exception):
    '''Raised when a player tries to do an invalid move.'''
    pass


class Game:
    '''The main class to manage a go game.'''
    def __init__(self, board_size: int) -> None:
        '''Initialize an empty game. Black should be first player to move.'''
        raise NotImplementedError

    def play_move(self, x: int, y: int) -> None:
        '''The current player makes a move.

        (x=-1,y=-1) means that the current player skips their turn.
        Any other value means that the current player wants to add a stone at (x,y).

        This method should raise an InvalidMoveError if the move is invalid.
        This method may raise other exceptions (e.g., when playing a move while the game is already finished).
        Otherwise, the move should be applied on the board.
        '''
        raise NotImplementedError
